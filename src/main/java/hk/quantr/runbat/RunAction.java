/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/NetBeansModuleDevelopment-files/contextAction.java to edit this template
 */
package hk.quantr.runbat;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle.Messages;

@ActionID(
		category = "Bugtracking",
		id = "hk.quantr.runbat.RunAction"
)
@ActionRegistration(
		iconBase = "hk/quantr/runbat/application_xp_terminal.png",
		displayName = "#CTL_RunAction"
)
@ActionReferences({
	//	@ActionReference(path = "Menu/File", position = 0),
	//	@ActionReference(path = "Toolbars/File", position = 0),
	@ActionReference(path = "Loaders/text/x-bat/Actions", position = 40000),
	@ActionReference(path = "Editors/text/x-bat/Popup", position = 400)
})
@Messages("CTL_RunAction=Run Bat")
public final class RunAction implements ActionListener {

	private final RunBatDataObject context;

	public RunAction(RunBatDataObject context) {
		this.context = context;
	}

	@Override
	public void actionPerformed(ActionEvent ev) {
		try {
			File file = new File(context.getPrimaryFile().getPath());
			Runtime.getRuntime().exec("cmd /c start " + context.getPrimaryFile().getPath(), null, file.getParentFile());
		} catch (IOException ex) {
			Exceptions.printStackTrace(ex);
		}
	}
}
